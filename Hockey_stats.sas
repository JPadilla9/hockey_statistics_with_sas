/*Jennie Padilla
03/28/2019*/
data Examtwo;
infile '/folders/myfolders/CSCI3113/xcelfiles_csvfiles/Hockey_data.csv' dlm=',' firstobs=2;
LENGTH Team $10;
input Team $ Conference $ GP G GA SVPCT percent6. PIM;
goals_per_game = G/GP;
run;
proc sort;
by Conference;
proc means mean;
by Conference;
var SVPCT;
output out = new 
	mean(SVPCT)=Average_Save_Percentage;
format Average_Save_Percentage: percent9.4;
RUN;
proc sort data = new;
	by Conference;
Data merging;
	merge Examtwo new;
	by Conference;
	drop _freq_;
	drop _type_;
run;
proc print data = merging;
run;

data finally;
set merging;
proc sort;
by Team;
%macro work(name=,moreless=,number=);
data ahh;
set finally;
if &name &moreless &number;
run;
PROC PRINT DATA = ahh;
run;
%mend work;
%work(name = PIM, moreless= >=, number=500);
%work(name=GA, moreless= <, number=170); 

*Histogram and Boxplot;
proc sgplot data = merging;
	histogram G/ Group=Conference scale=count;
	title 'Histogram of goals grouped by conference';
	run;
proc sgplot data = merging;
	reg x = Average_Save_Percentage y=GA;
	title 'regression line of save percentage vs goals allowed';
	run;
	
proc sgplot data = merging;
	vbox PIM;
	TITLE'BOXPLOT OF PIM';
	RUN;
	


