# Hockey_Statistics_with_SAS

This project was intended to display some of the skills I have aquired using SAS. 

First, the program reads in a csv file that contains statistics for some hockey teams.

## Key
* GP = games played
* GA = number of goals allowed
* SVPCT = fraction of goals stopped ((S-GA)/SA)
* G = total goals
* PIM = penalty minutes

I then created a new column called goals_per_game by dividing G by GP. I created another new column by calculating the mean of SVPCT sorted by conference(east or west). I merged this column into the dataset.

## Macro
* I created a macro that can be used to separate certain statistics. In this particular one, I printed a table with the stipulation that PIM was greater than or equal to 500.
* A second print out was of the team's statistics where the goals allowed was less than 170.

## Histogram
* I created a histogram of goals grouped by conference.

## Regression Line
* I displayed a regression line of save percentages vs goals allowed.

## Boxplot
* Box plot of PIM

> I have also included the results of the program.